package com.barzev;

import com.barzev.scraper.exception.NotFoundException;
import com.barzev.scraper.model.Result;
import com.barzev.scraper.parser.SainsburysParser;

import java.io.IOException;

/**
 * This main app runner provides a command-line interface to trigger the parser
 */
public class Runner {

    private final static int DEFAULT_CONNECTION_TIMEOUT_MS = 1000;

    private static SainsburysParser parser;

    public static void main(String[] args) throws IOException, NotFoundException {
        Result output;
        switch (args.length) {
            case 0:
                help();
                return;
            case 1:
                output = runOneArg(args[0]);
                System.out.println(parser.toJSON(output, true));
                break;
            case 2:
                try {
                    output = runTwoArgs(args[0], args[1]);
                    System.out.println(parser.toJSON(output, true));
                    break;
                } catch (IOException ex) {
                    break;
                }
            default:
                help();
                break;
        }
    }

    /**
     * Runs the parser with 1 URL argument and the default connection timeout value
     *
     * @param url the URL to scrape from
     * @return Result the parsed result
     * @throws IOException       on error
     * @throws NotFoundException on error
     */
    private static Result runOneArg(String url) throws IOException, NotFoundException {
        parser = new SainsburysParser(DEFAULT_CONNECTION_TIMEOUT_MS); // default
        return parser.run(url);
    }

    /**
     * Runs the parser when the user has supplied 2 arguments
     *
     * @param url               the URL to scrape from
     * @param connectionTimeout the HTTP Connection timeout allowed
     * @return Result the parsed result
     * @throws IOException       on error
     * @throws NotFoundException on error
     */
    private static Result runTwoArgs(String url, String connectionTimeout) throws IOException, NotFoundException {
        if (connectionTimeout.matches("-?\\d+")) { // is an integer
            parser = new SainsburysParser(Integer.parseInt(connectionTimeout)); // default
            return parser.run(url);
        } else {
            System.out.println("Please supply a valid numerical value as the second argument, e.g. \"1000\".");
            throw new IOException();
        }
    }

    /**
     * Prints helpful information on how to run the program
     */
    private static void help() {
        System.out.println("------ Help ------ ");
        System.out.println("To run this application please use gradle or the supplied gradle wrapper - \"./gradlew\" ");
        System.out.println("-- List of arguments -- ");
        System.out.println("1. URL  - String");
        System.out.println("2. [connectionTimeout] - int, OPTIONAL");
        System.out.println("-- Example --");
        System.out.println("./gradlew run -PappArgs=\"['arg1', 'args2']\"");
        System.out.println("-OR-");
        System.out.println("./gradlew run -PappArgs=\"['arg1', 'args2']\"");
    }
}
