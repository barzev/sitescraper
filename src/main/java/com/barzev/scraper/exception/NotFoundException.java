package com.barzev.scraper.exception;

/**
 * Thrown when no items have been found
 */
public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }
}
