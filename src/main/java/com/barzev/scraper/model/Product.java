package com.barzev.scraper.model;

public class Product {

    private final String title;
    private final Integer kCalPer100g;
    private final double unitPrice;
    private final  String description;

    public Product(String title, Integer kCalPer100g, double unitPrice, String description) {
        this.title = title;
        this.kCalPer100g = kCalPer100g;
        this.unitPrice = unitPrice;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public double getUnitPrice() {
        return unitPrice;
    }
}
