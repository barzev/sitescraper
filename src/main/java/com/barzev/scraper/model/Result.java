package com.barzev.scraper.model;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private List<Product> products;
    private String total;

    public Result() {
        products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public String getTotal() {
        return total;
    }
    public void addProduct(Product product) {
        products.add(product);
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
