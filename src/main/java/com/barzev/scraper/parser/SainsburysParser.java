package com.barzev.scraper.parser;

import com.barzev.scraper.exception.NotFoundException;
import com.barzev.scraper.model.Product;
import com.barzev.scraper.model.Result;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

/**
 * A specific implementation of the Parser class. This class represents a Sainsbury's parser that goes through a page,
 * scrapes all relevant product details by using pre-setup class names
 */
public class SainsburysParser extends Parser {

    // Relevant HTML Element class names, tags and strings
    private final static String UNORDERED_LIST_CLASS_NAMES = "gridView";
    private final static String LIST_ITEM_CLASS_NAME = "gridItem";
    private final static String UNIT_PRICE_CLASS_NAME = "pricePerUnit";
    private final static String KCAL_STRING = "kcal";
    private final static String PRODUCT_ITEM_HEADER_TAG = "h3";
    private final static String PRODUCT_TITLE_CLASS_NAME = "productNameAndPromotions";


    public SainsburysParser(int parserTimeout) {
        super(parserTimeout);
    }

    /**
     * A useful main method that can be used to run the application easily from an IDE
     *
     * @param args supplied arguments
     * @throws IOException       on error
     * @throws NotFoundException on error
     */
    private static void main(String[] args) throws IOException, NotFoundException {
        SainsburysParser sainsburysParser = new SainsburysParser(3000);
        sainsburysParser.run(args[0]);
    }

    /**
     * Gets the list item class name
     * @return the list item class name
     */
    public static String getListItemClassName() {
        return LIST_ITEM_CLASS_NAME;
    }

    /**
     * Gets the unit price class name
     * @return the unit price class name
     */
    public static String getUnitPriceClassName() {
        return UNIT_PRICE_CLASS_NAME;
    }

    /**
     * Runs the parser at a given URL location. Scrapes the page, collates all data and returns it
     * in the needed Result object.
     *
     * @param url the url where the scraper will be run at
     */
    public Result run(String url) throws IOException, NotFoundException {
        Document document = getHtmlDocument(url); // root html page/document
        return collectProductData(getProductsList(document));
    }

    /**
     * Retrieves a list of `li` html Elements that contain all LIST_ITEM_CLASS_NAME-identified items
     *
     * @param document the html to scrape
     * @return a list of html Elements that contain all "gridItem" items
     * @throws NotFoundException on error, when the provided CSS class names do not match any Elements
     */
    public Elements getProductsList(Document document) throws NotFoundException {
        Element ul = getFirstElementByClassName(document, UNORDERED_LIST_CLASS_NAMES);
        return ul.getElementsByClass(LIST_ITEM_CLASS_NAME);
    }

    /**
     * Collects all relevant product data by constructing the Result object with every necessary piece of data
     *
     * @param productListItems the list of all <li></li> product Elements
     * @return the Result object containing the final product data
     * @throws IOException on error
     */
    public Result collectProductData(Elements productListItems) throws IOException {
        Result result = new Result();
        for (Element productListItem : productListItems) {
            result.addProduct(new Product(
                    getTitle(productListItem),
                    getKcalPer100g(productListItem),
                    getUnitPrice(productListItem),
                    getProductDescription(productListItem)
            ));
        }

        result.setTotal(String.format("%.2f", calculateTotal(result.getProducts())));
        return result;
    }


    /**
     * Retrieves a product's title by accessing the anchor and extracting the text
     *
     * @param productListItem the `li` product Element
     * @return the extracted title string
     */
    public String getTitle(Element productListItem) {
        Element anchor = getProductAnchor(productListItem);
        return anchor.text();
    }

    /**
     * Retrieves a single `li` product item's unit price
     *
     * @param productListItem the `li` product Element
     * @return the unit price
     */
    public double getUnitPrice(Element productListItem) {
        String price = productListItem.getElementsByClass(UNIT_PRICE_CLASS_NAME).get(0).text();
        return Double.parseDouble(price.trim().substring(1).replace("/unit", ""));
    }

    /**
     * Retrieves the Kcal per 100g of a specific product. It looks for elements containing "kcal". Returns the
     * highest found such element. Utilises Integer wrapper so that null values can be stored. Such values are
     * omitted when generating the Result JSON.
     *
     * @param productListItem the `li` product Element to be traversed and analysed
     * @return the extracted Kcal per 100g for a specific product
     * @throws IOException on error
     */
    public Integer getKcalPer100g(Element productListItem) throws IOException {
        Document productPage = getPageByProductAnchor(productListItem);
        Elements allContainingKcal = productPage.getElementsContainingOwnText(KCAL_STRING);
        allContainingKcal.removeIf(elem -> elem.text().length() > 10); // filter out guide/info elements
        int highestFound = 0; // if multiple elements found, will look for highest found as kcal per 100 g > per serving

        if (allContainingKcal.size() == 0) return null; // none found
        if (allContainingKcal.size() < 2) { // if only 1 element found take it
            return Integer.parseInt(allContainingKcal.text().trim().replace("kcal", ""));
        } else {
            for (Element containsKcal : allContainingKcal) {
                int foundValue = Integer.parseInt(containsKcal.text().trim().replace("kcal", ""));
                if (foundValue > highestFound) {
                    highestFound = foundValue;
                }
            }
        }
        return highestFound;
    }

    /**
     * Retrieves the first line of a product's description. It does so by going into the product item's page
     * and extracting the relevant information
     *
     * @param productListItem the `li` product Element to be traversed and analysed
     * @return the first line of the product's description
     * @throws IOException on error
     */
    public String getProductDescription(Element productListItem) throws IOException {
        Document productPage = getPageByProductAnchor(productListItem);
        Elements productHeaders = productPage.getElementsByTag(PRODUCT_ITEM_HEADER_TAG);

        for (Element element : productHeaders) {
            if (element.text().trim().equals("Description")) {
                for (Element child : element.nextElementSibling().children()) {
                    String foundText = child.text().trim();
                    if (foundText.length() > 0) {
                        return foundText;
                    }
                }
            }
        }
        return "";
    }

    /**
     * Calculates the total sum of all items
     *
     * @param products the list of Products to collect the total on
     * @return the total sum of all unit values
     */
    public double calculateTotal(List<Product> products) {
        double sum = 0;
        for (Product product : products) {
            sum += product.getUnitPrice();
        }
        return sum;
    }


    /**
     * Gets a specific product's HTML document by grabbing and following the `li` item's anchor
     *
     * @param productListItem the `li` product Element to be traversed and analysed
     * @return an HTML document that contains an actual product item's page
     * @throws IOException on error
     */
    public Document getPageByProductAnchor(Element productListItem) throws IOException {
        Element anchor = getProductAnchor(productListItem);
        return getHtmlDocument((anchor.absUrl("href")));
    }

    /**
     * Retrieves a product's anchor Element
     *
     * @param productItem the product list item to explore
     * @return the product anchor Element
     */
    public Element getProductAnchor(Element productItem) {
        Element nameDiv = productItem.getElementsByClass(PRODUCT_TITLE_CLASS_NAME).get(0); // 1 per product
        return nameDiv.getElementsByTag("a").get(0); // 1 per product
    }
}
