package com.barzev.scraper.parser;

import com.barzev.scraper.exception.NotFoundException;
import com.barzev.scraper.model.Result;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * A generic, extensible Parser that implements Jsoup and provides convenient high-level functions
 */
class Parser {

    private int timeoutMs;

    /**
     * A specific implementation of the Jsoup parser
     *
     * @param timeout the connection timeout used by the HttpConnection
     */
    Parser(int timeout) {
        this.timeoutMs = timeout;
    }

    /**
     * Given the Parser's source URL and timeout, extract the HTML document
     *
     * @return the extracted HTML document
     * @throws IOException on error
     */
    Document getHtmlDocument(String sourceUrl) throws IOException {
        return Jsoup.connect(sourceUrl).timeout(timeoutMs).get();
    }

    /**
     * Finds the first Element that contains a specific classname / multiple classnames matching the supplied input
     * String
     *
     * @param source    the html source to look in
     * @param className the class names to identify the Element by
     * @return the Element found after the traversal procedure
     */
    Element getFirstElementByClassName(Document source, String className) throws NotFoundException {
        Elements found = source.getElementsByClass(className);
        if (found.size() < 1) {
            throw new NotFoundException("No such element containing class name: \"" + className + "\" found. ");
        }
        return found.get(0); // return first found
    }

    /**
     * A convenience method that
     *
     * @param result              the Result object that contains the final product data
     * @param disableHtmlEscaping whether the HTML should be escaped
     * @return the generated JSON string
     */
    public String toJSON(Result result, boolean disableHtmlEscaping) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson;
        if (disableHtmlEscaping) {
            gson = gsonBuilder.disableHtmlEscaping().create();
        } else {
            gson = gsonBuilder.create();
        }
        return gson.toJson(result);
    }
}
