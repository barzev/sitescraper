package com.barzev.scraper.parser;

import com.barzev.scraper.exception.NotFoundException;
import com.barzev.scraper.model.Product;
import com.barzev.scraper.model.Result;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.barzev.scraper.parser.SainsburysParser.getListItemClassName;
import static com.barzev.scraper.parser.SainsburysParser.getUnitPriceClassName;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Mockito.*;

public class SainsburysParserTest {

    private SainsburysParser spySainsburysParser;
    private Document mainHtml;
    private Document productHtml;
    private Elements someProductListItems;

    private final int TIMEOUT = 3000;
    private final String SOME_URL = "SOME URL";

    @Before
    public void setUp() throws Exception {
        File mainPage = new File("src/test/java/com/barzev/scraper/html/sainsburys.html");
        File productPage = new File("src/test/java/com/barzev/scraper/html/blueberries.html");
        mainHtml = Jsoup.parse(mainPage, null);
        productHtml = Jsoup.parse(productPage, null);
        spySainsburysParser = spy(new SainsburysParser(TIMEOUT));
        someProductListItems = spySainsburysParser.getProductsList(mainHtml);

        // inject the spy with a html document read from file
        doReturn(mainHtml).when(spySainsburysParser).getHtmlDocument(SOME_URL);
    }

    @Test
    public void testRunCallsGetHtmlDocumentOnce() throws IOException, NotFoundException {
        doReturn(new Result()).when(spySainsburysParser).collectProductData(any(Elements.class));
        spySainsburysParser.run(SOME_URL);

        verify(spySainsburysParser, times(1)).getHtmlDocument(SOME_URL);
    }

    @Test
    public void testRunCallsCollectProductDataOnce() throws IOException, NotFoundException {
        doReturn(new Result()).when(spySainsburysParser).collectProductData(any(Elements.class));
        spySainsburysParser.run(SOME_URL);

        verify(spySainsburysParser, times(1)).collectProductData(any(Elements.class));
    }


    @Test
    public void testRunReturnsAResultWithExpectedData() throws IOException, NotFoundException {
        Result dummyResult = new Result();
        dummyResult.setTotal("10"); // a dummy Result object
        doReturn(dummyResult).when(spySainsburysParser).collectProductData(any(Elements.class));

        assertThat(spySainsburysParser.run(SOME_URL).getTotal(), is(dummyResult.getTotal()));
    }

    @Test
    public void testGetProductsListFindsElements() throws NotFoundException {
        Elements gridItems = spySainsburysParser.getProductsList(mainHtml);

        assertThat(gridItems.size(), is(not(0)));
    }

    @Test
    public void testGetProductsListFindsElementWithCorrectClass() throws NotFoundException {
        Elements gridItems = spySainsburysParser.getProductsList(mainHtml);

        assertThat(gridItems.get(0).className(), is(getListItemClassName()));
    }

    @Test
    public void testCollectProductDataGrabsDataAndReturnsAResult()
            throws NotFoundException, IOException {
        // when we try to access a product page, supply an actual static product html doc
        doReturn(productHtml).when(spySainsburysParser).getPageByProductAnchor(any(Element.class));
        someProductListItems = spySainsburysParser.getProductsList(mainHtml);
        Result result = spySainsburysParser.collectProductData(someProductListItems);

        assertThat(result.getProducts().size(), is(not(0)));
    }

    @Test
    public void testcollectProductDataReturnsAResultWithMeaningfulData()
            throws NotFoundException, IOException {
        doReturn(productHtml).when(spySainsburysParser).getPageByProductAnchor(any(Element.class));

        someProductListItems = spySainsburysParser.getProductsList(mainHtml);
        Result result = spySainsburysParser.collectProductData(someProductListItems);

        // every item contains "Sainsbury's" String in its title
        assertThat(result.getProducts().get(0).getTitle(), containsString("Sainsbury's"));
    }


    @Test
    public void testcollectProductDataReturnsAResultWithCorrectlyFormattedTotalPrice()
            throws NotFoundException, IOException {
        doReturn(productHtml).when(spySainsburysParser).getPageByProductAnchor(any(Element.class));

        someProductListItems = spySainsburysParser.getProductsList(mainHtml);
        Result result = spySainsburysParser.collectProductData(someProductListItems);

        // 2 decimal places and correctly formatted
        assertThat(result.getTotal(), is("39.50"));
    }

    @Test
    public void testGetTitleShouldGetACorrectTitle() {
        String retrievedTitle = spySainsburysParser.getTitle(someProductListItems.get(0));

        // every item contains "Sainsbury's" String in its title
        assertThat(retrievedTitle, containsString("Sainsbury's"));
    }

    @Test
    public void getUnitPriceShouldReturnCorrectValue() {
        Element unitPriceElement = someProductListItems.get(0).getElementsByClass(getUnitPriceClassName()).get(0);
        unitPriceElement.text("$1.61/unit");
        unitPriceElement.appendTo(someProductListItems.get(0));

        assertThat(spySainsburysParser.getUnitPrice(someProductListItems.get(0)), is(1.61));
    }

    @Test
    public void getKcalPer100gReturnsAValidValue() throws IOException {
        doReturn(productHtml).when(spySainsburysParser).getPageByProductAnchor(any(Element.class));
        Integer kCal = spySainsburysParser.getKcalPer100g(new Element("<tag>"));

        assertNotNull(kCal);
    }

    @Test
    public void getKcalPer100gCanReturnNullToBeOmittedInJSON() throws IOException {
        // inspect a non-product page, forcing it to parse a page without kCal specified anywhere
        doReturn(mainHtml).when(spySainsburysParser).getPageByProductAnchor(any(Element.class));

        Integer kCal = spySainsburysParser.getKcalPer100g(new Element("<tag>"));
        assertNull(kCal);
    }

    @Test
    public void getProductDescriptionReturnsAValidText() throws IOException {
        doReturn(productHtml).when(spySainsburysParser).getPageByProductAnchor(any(Element.class));
        String description = spySainsburysParser.getProductDescription(someProductListItems.get(0));

        // every/most description contains the "Sainsbury's" String
        assertThat(description, containsString("Sainsbury's"));
    }

    @Test
    public void testCalculateTotalReturnsCorrectSumAsADouble() {
        List<Product> products = new ArrayList<>();
        Product dummyProduct1 = new Product("", null, 1.37, "");

        products.add(dummyProduct1);
        products.add(dummyProduct1);

        // expected is 2 products of same kind, so 2 x unit price
        assertThat(spySainsburysParser.calculateTotal(products), is(2.74));
    }

    @Test
    public void testGetProductAnchor() {
        Element found = spySainsburysParser.getProductAnchor(someProductListItems.get(0));
        // it should find an anchor
        assertThat(found.getElementsByTag("a").size(), is(not(0)));
    }

    @Test
    public void getPageByProductAnchorSuccessfullyRetrievesADocument() throws IOException {
        final String GOOGLE_URL = "https://www.google.co.uk";
        Attribute attribute = new Attribute("href", GOOGLE_URL);
        Element anchorElement = new Element(Tag.valueOf("a"), "", new Attributes().put(attribute));

        doReturn(anchorElement).
                when(spySainsburysParser).getProductAnchor(someProductListItems.get(0));

        Document document = spySainsburysParser.getPageByProductAnchor(someProductListItems.get(0));

        assertThat(document.location(), is(GOOGLE_URL));
    }
}
