package com.barzev.scraper.parser;

import com.barzev.scraper.exception.NotFoundException;
import com.barzev.scraper.model.Product;
import com.barzev.scraper.model.Result;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ParserTest {

    private Parser parser;
    private Parser parserMock;
    private Document html;
    private final int TIMEOUT = 3000;
    private final String SOME_URL = "SOME URL";

    @Before
    public void setUp() throws Exception {
        parser = new Parser(TIMEOUT);
        mockDocumentResponse();
        html = parserMock.getHtmlDocument(SOME_URL);
    }

    /**
     * Mocks the Jsoup connect and get HTML request. Returns a local html file.
     * @throws IOException on error
     */
    private void mockDocumentResponse() throws IOException {
        File in = new File("src/test/java/com/barzev/scraper/html/msn.html"); // load html file

        parserMock = mock(Parser.class);
        when(parserMock.getHtmlDocument(SOME_URL)).thenReturn(Jsoup.parse(in, null));
    }

    @Test
    public void getHtmlDocumentReturnsADocument() {
        // returns an object of correct class
        assertThat(html, is(instanceOf(Document.class)));
    }

    @Test
    public void getHtmlDocumentAndContainsCorrectTitle() {
        // grab title and check if it contains a word we expect & not likely to ever change
        assertThat(html.head().getElementsByTag("title").text(), containsString("MSN"));
    }

    @Test
    public void getSpecificElemByClassNamesReturnsSomeElement() throws NotFoundException {
        String testClasses = "content";
        Element element = parser.getFirstElementByClassName(html, testClasses);

        assertThat(element.childNodeSize(), is(not(0)));
    }

    @Test
    public void getSpecificElemByMultipleClassNamesReturnsSomeElement() throws NotFoundException {
        String testClasses = "normalsection todaynavigation";
        Element element = parser.getFirstElementByClassName(html, testClasses);

        assertThat(element.childNodeSize(), is(not(0)));
    }

    @Test(expected=NotFoundException.class)
    public void getSpecificElemByClassNamesThrowsNotFoundExceptionWhenNoElementsFound() throws NotFoundException {
        String cssClass = "someInvalidClassName";
        Element element = parser.getFirstElementByClassName(html, cssClass);

        assertThat(element.childNodeSize(), is(not(0)));
    }

    @Test
    public void testToJSONWithNoHtmlEscaping() {
        Result dummyResult = new Result();
        dummyResult.addProduct(new Product("someTitle", 0, 0, ""));
        dummyResult.setTotal("10.00");

        String expectedJson = "{\"products\":[{\"title\":\"someTitle\",\"kCalPer100g\":0,\"unitPrice\":0.0,\"description\":\"\"}],\"total\":\"10.00\"}";
        String json = parser.toJSON(dummyResult, true);

        assertThat(json, is(expectedJson));
    }
}
