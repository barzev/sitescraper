### Author: Petar Barzev 

# Serverside-test

This project contains a Sainsbury's page scraper. It returns the relevant data in a JSON format. Contained are a list
of products, as well as the total price sum of all units.

## Dependencies

##### Main:
- Gradle
    - Chosen as it is very flexible and increases developer productivity by helping build, automate, bundle and deliver 
    dependencies quicker. What is more, it creates an easy-to-run interface for the application and tests alike.
- Jsoup
    - I have used this library in the past to interact with HTML documents. It's very powerful, well-built and flexible.
- Gson 
    - Widely-used, high-performance JSON mapper. Along with Jackson, it's the usual choice for such a job.

##### Test: 
- JUnit
    - The most widely-used unit testing framework. Completely sufficient in most cases.
- Hamcrest
    - Enhances readability, debugging, compile time errors, flexible
- Mockito
    - Great for spying on objects, mocking data and injecting data where needed.

*** Please note that all of these are handled by gradle, so no manual download/installation steps are necessary ***

## How to run app

- Go to the root of the project, i.e. SiteScraper/
- Run `./gradlew clean install build` clean up dependencies, install and build;
- Run `./gradlew run` to print running instructions;
- OR 
- Run `./gradlew run -PappArgs="['arg1', 'args2']"` where:
    - arg1 is the URL to run the scraper at;
    - arg2 is the HTTP connection timeout;
    
** Note, if running on linux, you might need to set execution flag, so that you have permission to run the gradlew, i.e.
    - Run `sudo chmod +x gradlew`.

## How to run tests

Run `gradle test`, or just `gradle build`, which also contains this task.
